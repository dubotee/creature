﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class BaseModel : MonoBehaviour
{
    public int laneID = 0;
    public DataController.CREATURE_SIDE side = DataController.CREATURE_SIDE.OWN;
    [HideInInspector]
    public bool isBlinking;
    // Use this for initialization

    void Start()
    {

    }

    float starBlinkingTime = 0.0f;
    float speed = 9.0f;

    void Update()
    {
        List<GameObject> opponents = side == DataController.CREATURE_SIDE.ENEMY ? DataController.instance.GameController.BattleLanes[Mathf.Clamp(laneID, 0, 1)].Creatures : DataController.instance.GameController.BattleLanes[Mathf.Clamp(laneID, 0, 1)].Enemies;
        bool isTargeting = false;
        foreach (GameObject opponent in opponents)
        {
            if (opponent.GetComponent<CreatureController>().TargetType == DataController.TARGET_TYPE.BASE)
            {
                isTargeting = true;
                break;
            }
        }

        if (isTargeting && !isBlinking)
        {
            isBlinking = true;
            starBlinkingTime = Time.time;
        }
        if (!isTargeting && isBlinking )
        {
            if (((Mathf.Sin((Time.time - starBlinkingTime) * speed)) + 1) / 2.0f >= 0.96f)
            {
                isBlinking = false;
            }
        }
        

        if (isBlinking)
        { 
            //GetComponent<Image>().color = new Color(1, (Mathf.Sin(Time.time * speed) + 2.5f) / 2.0f, (Mathf.Sin(Time.time * speed) + 1.0f) / 2.0f);
            GetComponent<Image>().color = new Color(1, 1f, 1f, ((Mathf.Cos((Time.time - starBlinkingTime) * speed)) + 1) / 2.0f);
        }
        else
        {
            GetComponent<Image>().color = new Color(1, 1f, 1f, 1f);
        }
    }
}
