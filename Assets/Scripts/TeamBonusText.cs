﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TeamBonusText : MonoBehaviour {
    CreatureData _host;
    public Color ownColor;
    public Color enemyColor;

    public CreatureData Host
    {
        get { return _host; }
        set
        {
            GetComponent<Text>().color = value.Side == DataController.CREATURE_SIDE.ENEMY ? enemyColor : ownColor;
            _host = value;
        }
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (_host != null)
        {

            transform.position = _host.transform.position + new Vector3(0.1f * Host.GetComponent<CreatureController>().SubLane, (Host.GetComponent<CreatureController>().SubLane == 0 ? -0.4f : 0f) * (Host.GetComponent<CreatureData>().Side == DataController.CREATURE_SIDE.ENEMY ? -1 : 1 ), 0);

            string text = "";
            text += DataController.instance.TEAM_ATTACK_BONUS_OVER_TIME > 0 && Host.GetComponent<CreatureController>().TeamAttackBonusCummalation > 0 ? ("+" + Mathf.RoundToInt(Host.GetComponent<CreatureController>().TeamAttackBonusCummalation * DataController.instance.TEAM_ATTACK_BONUS_OVER_TIME * 6) + " ATT\n") : "";
            text += DataController.instance.TEAM_ARMOR_BONUS_OVER_TIME > 0 && Host.GetComponent<CreatureController>().TeamDefendBonusCummalation > 0 ? ("+" + Mathf.RoundToInt(Host.GetComponent<CreatureController>().TeamDefendBonusCummalation * DataController.instance.TEAM_ARMOR_BONUS_OVER_TIME * 6) + " DEF\n") : "";
            //text += DataController.instance.TEAM_SPEED_BONUS_OVER_TIME > 0 ? ("+" + Host.TeamBonusCummalation * DataController.instance.TEAM_SPEED_BONUS_OVER_TIME + " SPD\n") : "";
            //text += DataController.instance.TEAM_DELAY_BONUS_OVER_TIME > 0 ? ("+" + Host.TeamBonusCummalation * DataController.instance.TEAM_DELAY_BONUS_OVER_TIME + " ASPD") : "";

            GetComponent<Text>().text = text;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    internal void BeginAnim()
    {
        //iTween.MoveFrom(gameObject, iTween.Hash("position", transform.position + new Vector3(0.05f, -0.2f, 0f), "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "Remove"));
    }

    void Remove()
    {
        //Destroy(this.gameObject);
    }
}
