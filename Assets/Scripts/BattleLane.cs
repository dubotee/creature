﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleLane
{
    Vector2 ownBasePos = Vector2.zero;
    Vector2 enemyBasePos = Vector2.zero;

    Base ownBase, enemyBase;

    List<GameObject> creatures;
    List<GameObject> enemies;

    public List<GameObject> Creatures
    {
        get { return creatures; }
    }

    public List<GameObject> Enemies
    {
        get { return enemies; }
    }

    public Base OwnBase
    {
        get { return ownBase; }
    }

    public Base EnemyBase
    {
        get { return enemyBase; }
    }

    public BattleLane(float x, Base ownBase, Base enemyBase)
    {
        creatures = new List<GameObject>();
        enemies = new List<GameObject>();

        this.ownBase = ownBase;
        this.enemyBase = enemyBase;

        ownBasePos = new Vector2(x, ownBase.YPos);
        enemyBasePos = new Vector2(x, enemyBase.YPos);

        //GameObject test = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //test.transform.position = ownBasePos;
        //GameObject test2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //test2.transform.position = enemyBasePos;
    }

    internal void SpawnCreature(DataController.CREATURE_SIDE side, int headCount, bool[] armData, bool[] legData, int armCount, int legCount, int shieldCount, int swordCount, int starCount)
    {
        GameObject newCreatue = null;
        Vector2 spawnPos = (side == DataController.CREATURE_SIDE.OWN) ? ownBasePos : enemyBasePos;
        List<GameObject> spawnList = (side == DataController.CREATURE_SIDE.OWN) ? creatures : enemies;
        int subLane = 0;

        bool[] subLaneOccupied = new bool[3];
        foreach (GameObject laneObj in spawnList)
        {
            switch (laneObj.GetComponent<CreatureController>().SubLane)
            {
                case 0: subLaneOccupied[1] = true;
                    break;
                case -1: subLaneOccupied[0] = true;
                    break;
                case 1: subLaneOccupied[2] = true;
                    break;
            }
        }

        if (!subLaneOccupied[1])
        {
            subLane = 0;
        }
        else if (!subLaneOccupied[2])
        {
            subLane = 1;
        }
        else
        {
            subLane = -1;
        }

        newCreatue = (GameObject)GameObject.Instantiate((side == DataController.CREATURE_SIDE.OWN) ? DataController.instance.GameController.backCreaturePrefab : DataController.instance.GameController.frontCreaturePrefab);
        spawnList.Add(newCreatue);
        newCreatue.transform.position = spawnPos + new Vector2(DataController.instance.LANE_OFFSET * subLane, 0f);

        newCreatue.GetComponent<CreatureController>().Lane = this;
        newCreatue.GetComponent<CreatureController>().SetCreatureData(side, headCount, armData, legData, subLane, shieldCount, swordCount, starCount);
    }
}
