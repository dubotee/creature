﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent(typeof(UIController))]
public class GameController : MonoBehaviour
{
    List<PartTile> initPartTiles;
    List<PartTile> initPartTilesBuffer;

    Lane[] gridLanes;
    BattleLane[] battleLanes;
    Base ownBase;
    Base enemyBase;

    int playerHead = 10;
    int playerArm = 20;
    int playerLeg = 20;
    int playerSword = 2;
    int playerShield = 2;

    public GameObject partTilePrefab;
    public GameObject previewPrefab;
    public GameObject backCreaturePrefab;
    public GameObject frontCreaturePrefab;
    public GameObject selectionNodePrefab;
    public GameObject selectionPathPrefab;

    bool isSelecting;
    bool isBoardDeslected;
    GameObject deletingCreature = null;
    float deleteingFirstClickTime;
    List<PartTile> selectingTiles;
    List<GameObject> selectionPaths;
    List<GameObject> selectionNodes;

    private GameObject characterPreview;

    public BattleLane[] BattleLanes
    {
        get { return battleLanes; }
    }

    public Base OwnBase
    {
        get { return ownBase; }
    }

    public Base EnemyBase
    {
        get { return enemyBase; }
    }

    public int PlayerHead
    {
        get { return playerHead; }
    }
    public int PlayerArm
    {
        get { return playerArm; }
    }
    public int PlayerLeg
    {
        get { return playerLeg; }
    }

    public int PlayerSword
    {
        get { return playerSword; }
    }

    public int PlayerShield
    {
        get { return playerShield; }
    }


    // Use this for initialization
    void Awake()
    {
        DataController.instance.GameController = this;
        DataController.instance.CalculateScreenSize();
        selectingTiles = new List<PartTile>();

        initPartTiles = new List<PartTile>();
        initPartTilesBuffer = new List<PartTile>();
        gridLanes = new Lane[4];
        battleLanes = new BattleLane[2];

        ownBase = new Base(DataController.CREATURE_SIDE.OWN, DataController.instance.BaseOwnY);
        enemyBase = new Base(DataController.CREATURE_SIDE.ENEMY, DataController.instance.BaseEnemyY);

        battleLanes[0] = new BattleLane(DataController.instance.BaseLeftX, ownBase, enemyBase);
        battleLanes[1] = new BattleLane(DataController.instance.BaseRightX, ownBase, enemyBase);

        //Shuffle 50 start tiles
        playerHead = DataController.instance.TOTAL_MONSTER;
        playerArm = DataController.instance.TOTAL_MONSTER * 2;
        playerLeg = DataController.instance.TOTAL_MONSTER * 2;
        playerSword = DataController.instance.TOTAL_POWERUP_SWORD;
        playerShield = DataController.instance.TOTAL_POWERUP_SHIELD;

        int randomStartingHead = Random.Range(4, 7);
        int randomStartingArm = Random.Range(10, 14);
        int randomStartingSword = Mathf.Clamp(Random.Range(0, 2), 0, DataController.instance.TOTAL_POWERUP_SWORD);
        int randomStartingShield = Mathf.Clamp(Random.Range(0, 2), 0, DataController.instance.TOTAL_POWERUP_SHIELD);
        int randomStartingStar = Mathf.Clamp(Random.Range(0, 2), 0, DataController.instance.TOTAL_POWERUP_STAR);

        int randomStartingLeg = 8 * 4 - randomStartingArm - randomStartingHead - randomStartingShield - randomStartingSword - randomStartingStar;

        for (int i = 0; i < playerHead; i++)
        {
            GameObject newTileObj = (GameObject)Instantiate(partTilePrefab, transform.position, Quaternion.identity);
            newTileObj.GetComponent<PartTile>().Type = PartTile.PART_TYPE.HEAD;
            if (i < randomStartingHead)
                initPartTiles.Add(newTileObj.GetComponent<PartTile>());
            else
                initPartTilesBuffer.Add(newTileObj.GetComponent<PartTile>());
        }
        for (int i = 0; i < playerArm; i++)
        {
            GameObject newTileObj = (GameObject)Instantiate(partTilePrefab, transform.position, Quaternion.identity);
            newTileObj.GetComponent<PartTile>().Type = PartTile.PART_TYPE.ARM;
            if (i < randomStartingArm)
                initPartTiles.Add(newTileObj.GetComponent<PartTile>());
            else
                initPartTilesBuffer.Add(newTileObj.GetComponent<PartTile>());
        }
        for (int i = 0; i < playerLeg; i++)
        {
            GameObject newTileObj = (GameObject)Instantiate(partTilePrefab, transform.position, Quaternion.identity);
            newTileObj.GetComponent<PartTile>().Type = PartTile.PART_TYPE.LEG;
            if (i < randomStartingLeg)
                initPartTiles.Add(newTileObj.GetComponent<PartTile>());
            else
                initPartTilesBuffer.Add(newTileObj.GetComponent<PartTile>());
        }
        for (int i = 0; i < playerSword; i++)
        {
            GameObject newTileObj = (GameObject)Instantiate(partTilePrefab, transform.position, Quaternion.identity);
            newTileObj.GetComponent<PartTile>().Type = PartTile.PART_TYPE.SWORD;
            if (i < randomStartingSword)
                initPartTiles.Add(newTileObj.GetComponent<PartTile>());
            else
                initPartTilesBuffer.Add(newTileObj.GetComponent<PartTile>());
        }
        for (int i = 0; i < playerShield; i++)
        {
            GameObject newTileObj = (GameObject)Instantiate(partTilePrefab, transform.position, Quaternion.identity);
            newTileObj.GetComponent<PartTile>().Type = PartTile.PART_TYPE.SHIELD;
            if (i < randomStartingShield)
                initPartTiles.Add(newTileObj.GetComponent<PartTile>());
            else
                initPartTilesBuffer.Add(newTileObj.GetComponent<PartTile>());
        }
        for (int i = 0; i < DataController.instance.TOTAL_POWERUP_STAR; i++)
        {
            GameObject newTileObj = (GameObject)Instantiate(partTilePrefab, transform.position, Quaternion.identity);
            newTileObj.GetComponent<PartTile>().Type = PartTile.PART_TYPE.STAR;
            if (i < randomStartingStar)
                initPartTiles.Add(newTileObj.GetComponent<PartTile>());
            else
                initPartTilesBuffer.Add(newTileObj.GetComponent<PartTile>());
        }

        bool isShuffleEligible = false;
        while (!isShuffleEligible)
        {
            initPartTiles.Shuffle<PartTile>();

            //Check Head
            bool[] isHeadLance = new bool[4];
            for (int i = 0; i < 4 * 8; i++)
            {
                if (initPartTiles[i].Type == PartTile.PART_TYPE.HEAD)
                {
                    isHeadLance[i % 4] = true;
                }
            }
            isShuffleEligible = isHeadLance[0] && isHeadLance[1] && isHeadLance[2] && isHeadLance[3];

            //Check arm
            if (isShuffleEligible)
            {
                for (int i = 0; i < 8 - 1; i++)
                {
                    int armCount = 0;
                    for (int ii = i * 4; ii < (i + 2) * 4; ii++)
                    {
                        if (initPartTiles[ii].Type == PartTile.PART_TYPE.ARM) armCount++;
                    }
                    if (armCount > 4)
                    {
                        isShuffleEligible = false;
                        break;
                    }
                }
            }

            if (isShuffleEligible)
            {
                for (int i = 0; i < initPartTiles.Count; i++)
                {
                    if (initPartTiles[i].isSpecialTile)
                    {
                        if (i > 0)
                        {
                            if (initPartTiles[i - 1].isSpecialTile)
                            {
                                isShuffleEligible = false;
                                break;
                            }
                        }
                        if (i > 3)
                        {
                            if (initPartTiles[i - 4].isSpecialTile)
                            {
                                isShuffleEligible = false;
                                break;
                            }
                        }
                        if (i < initPartTiles.Count - 1)
                        {
                            if (initPartTiles[i + 1].isSpecialTile)
                            {
                                isShuffleEligible = false;
                                break;
                            }
                        }
                        if (i < initPartTiles.Count - 5)
                        {
                            if (initPartTiles[i + 4].isSpecialTile)
                            {
                                isShuffleEligible = false;
                                break;
                            }
                        }
                    }
                }
            }
        }

        initPartTilesBuffer.Shuffle<PartTile>();
        List<PartTile> temp_initPartTiles = initPartTiles;
        initPartTiles = new List<PartTile>(temp_initPartTiles.Count + initPartTilesBuffer.Count);
        initPartTiles.AddRange(temp_initPartTiles);
        initPartTiles.AddRange(initPartTilesBuffer);

        selectionPaths = new List<GameObject>();
        selectionNodes = new List<GameObject>();

        for (int i = 0; i < 4; i++)
        {
            gridLanes[i] = new Lane(i);
        }
        int j = 0;
        for (int i = 0; i < initPartTiles.Count; i++)
        {
            gridLanes[j].Add(initPartTiles[i]);
            initPartTiles[i].SetUpInitPoisition(gridLanes[j], gridLanes[j].IndexOf(initPartTiles[i]));
            j = (j + 1) % 4;
        }
    }

    IEnumerator Start()
    {
        yield return StartCoroutine(StartAllowed(3.0F));
    }

    IEnumerator StartAllowed(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        isStartAllowed = true;
    }

    bool isStartAllowed;
    // Update is called once per frame
    void Update()
    {
        if (!isStartAllowed) return;
        //Check to deselect board
        if (!isBoardDeslected && battleLanes[0].Creatures.Count + battleLanes[1].Creatures.Count >= 3)
        {
            foreach (Lane gridLane in gridLanes)
            {
                foreach (PartTile tile in gridLane)
                {
                    tile.IsDisabled = true;
                }
            }
            isBoardDeslected = true;
        }
        else if (isBoardDeslected && battleLanes[0].Creatures.Count + battleLanes[1].Creatures.Count < 3)
        {

            foreach (Lane gridLane in gridLanes)
            {
                foreach (PartTile tile in gridLane)
                {
                    tile.IsDisabled = false;
                }
            }
            isBoardDeslected = false;
        }

        //Usual Update
        bool hasObstacleRemoved = false;
        foreach (Lane gridLane in gridLanes)
        {
            for (int i = gridLane.Count - 1; i >= 0; i--)
            {
                PartTile tile = gridLane[i];
                if (tile.Type == PartTile.PART_TYPE.OBSTACLE)
                {
                    tile.CollapsedTime -= Time.deltaTime;
                    if (tile.CollapsedTime <= 0)
                    {
                        gridLane.Remove(tile);
                        hasObstacleRemoved = true;
                        Destroy(tile.gameObject);
                    }
                }
            }
        }
        if (hasObstacleRemoved)
        {
            CheckEmtyCenterLanes();
            ResetSelection();
            foreach (Lane gridLane in gridLanes)
            {
                gridLane.Refresh();
            }
        }
        //

        //Activate selection state
        if (Input.GetMouseButtonDown(0) && !isBoardDeslected)
        {
            isSelecting = true;
            if (characterPreview) Destroy(characterPreview);

            characterPreview = (GameObject)Instantiate(previewPrefab, new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, -100), Quaternion.identity);

            GameObject characterPreviewText = (GameObject)Instantiate(GetComponent<UIController>().previewInfoTextPrefab, new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, -2), Quaternion.identity);
            characterPreviewText.transform.parent = GetComponent<UIController>().damageCanvas.transform;
            characterPreviewText.transform.localScale = new Vector3(1, 1, 1);
            characterPreviewText.GetComponent<PreviewInfoText>().Host = characterPreview;
        }
        ////

        //Finish seletion state
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 endPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            bool isSwipingLeft = false;
            bool isSwipingRight = false;

            if (characterPreview != null)
            {
                bool isPerfect = characterPreview.GetComponent<CreatureData>().ArmCount == 2 && characterPreview.GetComponent<CreatureData>().LegCount == 2 && characterPreview.GetComponent<CreatureData>().HeadCount == 1;

                if (selectingTiles.Count == 5 && endPosition.x < DataController.instance.BaseLeftX + DataController.instance.TileSize * DataController.instance.BATTLE_LANE_RATE / 2) // swipe left
                {
                    isSwipingLeft = true;
                    SpawnCreature(0, DataController.CREATURE_SIDE.OWN, characterPreview.GetComponent<CreatureData>().HeadCount, characterPreview.GetComponent<CreaturePreview>().ArmSlot, characterPreview.GetComponent<CreaturePreview>().LegSlot,
                        characterPreview.GetComponent<CreatureData>().ArmCount, characterPreview.GetComponent<CreatureData>().LegCount,
                        characterPreview.GetComponent<CreatureData>().ShieldCount, characterPreview.GetComponent<CreatureData>().SwordCount, characterPreview.GetComponent<CreatureData>().StarCount);
                    playerHead -= characterPreview.GetComponent<CreatureData>().HeadCount;
                    playerArm -= characterPreview.GetComponent<CreatureData>().ArmCount;
                    playerLeg -= characterPreview.GetComponent<CreatureData>().LegCount;
                    playerSword -= characterPreview.GetComponent<CreatureData>().SwordCount;
                    playerShield -= characterPreview.GetComponent<CreatureData>().ShieldCount;
                }
                else if (selectingTiles.Count == 5 && endPosition.x > DataController.instance.BaseRightX - DataController.instance.TileSize * DataController.instance.BATTLE_LANE_RATE / 2) // swipe right
                {
                    isSwipingRight = true;
                    SpawnCreature(1, DataController.CREATURE_SIDE.OWN, characterPreview.GetComponent<CreatureData>().HeadCount, characterPreview.GetComponent<CreaturePreview>().ArmSlot, characterPreview.GetComponent<CreaturePreview>().LegSlot,
                        characterPreview.GetComponent<CreatureData>().ArmCount, characterPreview.GetComponent<CreatureData>().LegCount,
                        characterPreview.GetComponent<CreatureData>().ShieldCount, characterPreview.GetComponent<CreatureData>().SwordCount, characterPreview.GetComponent<CreatureData>().StarCount);
                    playerHead -= characterPreview.GetComponent<CreatureData>().HeadCount;
                    playerArm -= characterPreview.GetComponent<CreatureData>().ArmCount;
                    playerLeg -= characterPreview.GetComponent<CreatureData>().LegCount;
                    playerSword -= characterPreview.GetComponent<CreatureData>().SwordCount;
                    playerShield -= characterPreview.GetComponent<CreatureData>().ShieldCount;
                }

                if (selectingTiles.Count == 5 && (isSwipingLeft || isSwipingRight))
                {
                    foreach (PartTile checkingTile in selectingTiles)
                    {
                        if (isPerfect)
                        {
                            GetComponent<UIController>().ShowPerfectText(endPosition);
                        }
                        checkingTile.Lance.Remove(checkingTile);
                        initPartTiles.Remove(checkingTile);

                        if (checkingTile.Type == PartTile.PART_TYPE.SHIELD || checkingTile.Type == PartTile.PART_TYPE.SWORD)
                        {
                            GetComponent<UIController>().ShowSpecialTileText(checkingTile.transform.position, checkingTile.Type);
                        }
                        Destroy(checkingTile.gameObject);
                    }

                    CheckEmtyCenterLanes();
                }

                ResetSelection();
            }

            foreach (GameObject ally in battleLanes[0].Creatures)
            {
                endPosition = new Vector3(endPosition.x, endPosition.y, ally.transform.position.y);
                if (ally.GetComponent<BoxCollider2D>().bounds.Contains(endPosition))
                {
                    if (deletingCreature == null || deletingCreature != ally)
                    {
                        deleteingFirstClickTime = Time.time;
                        deletingCreature = ally;
                    }
                    else if (Time.time - deleteingFirstClickTime > 0.5f)
                    {
                        deleteingFirstClickTime = Time.time;
                    }
                    else
                    {
                        ally.GetComponent<CreatureController>().Kill();
                    }
                    break;
                }
            }
            foreach (GameObject ally in battleLanes[1].Creatures)
            {
                endPosition = new Vector3(endPosition.x, endPosition.y, ally.transform.position.y);
                if (ally.GetComponent<BoxCollider2D>().bounds.Contains(endPosition))
                {
                    if (deletingCreature == null || deletingCreature != ally)
                    {
                        deleteingFirstClickTime = Time.time;
                        deletingCreature = ally;
                    }
                    else if (Time.time - deleteingFirstClickTime > 0.5f)
                    {
                        deleteingFirstClickTime = Time.time;
                    }
                    else
                    {
                        ally.GetComponent<CreatureController>().Kill();
                    }
                    break;
                }
            }
        }
        ////

        //On selection state
        if (isSelecting)
        {
            Vector3 mouseWorldPos = Camera.allCameras[0].ScreenToWorldPoint(Input.mousePosition);
            mouseWorldPos = new Vector3(mouseWorldPos.x, mouseWorldPos.y, 0f);

            characterPreview.SetActive(selectingTiles.Count > 0);
            //characterPreview.transform.position = mouseWorldPos + new Vector3(mouseWorldPos.x == 0 ? 0 : (-Mathf.Abs(mouseWorldPos.x) / mouseWorldPos.x), 1, -2f);
            characterPreview.transform.position = mouseWorldPos + new Vector3(0, 1, -100f);

            PartTile previousselectingTile = null;
            foreach (Lane lane in gridLanes)
            {
                foreach (PartTile checkingTile in lane)
                {
                    if (checkingTile.gameObject.activeSelf)
                    {
                        if (checkingTile.GetComponent<BoxCollider2D>().bounds.Contains(mouseWorldPos))
                        {
                            if (!selectingTiles.Contains(checkingTile))
                            {
                                if (selectingTiles.Count > 0)
                                {
                                    previousselectingTile = selectingTiles[selectingTiles.Count - 1];
                                }
                                else
                                {
                                    previousselectingTile = checkingTile;
                                }
                                if (Mathf.Abs(previousselectingTile.Lance.LanceId - checkingTile.Lance.LanceId) <= 1 && Mathf.Abs(previousselectingTile.Pos - checkingTile.Pos) <= 1 && checkingTile.Type != PartTile.PART_TYPE.OBSTACLE)
                                {
                                    if (selectingTiles.Count < 5)
                                    {
                                        checkingTile.GetComponent<SpriteRenderer>().color = Color.green;
                                        selectingTiles.Add(checkingTile);

                                        GameObject node = (GameObject)Instantiate(selectionNodePrefab, checkingTile.transform.position + new Vector3(0, 0, -1f), Quaternion.identity);
                                        selectionNodes.Add(node);

                                        if (selectingTiles.Count > 1)
                                        {
                                            GameObject path = (GameObject)Instantiate(selectionPathPrefab, selectingTiles[selectingTiles.Count - 2].transform.position + (selectingTiles[selectingTiles.Count - 1].transform.position - selectingTiles[selectingTiles.Count - 2].transform.position) / 2, Quaternion.identity);

                                            if (selectingTiles[selectingTiles.Count - 2].transform.position.x == selectingTiles[selectingTiles.Count - 1].transform.position.x)
                                            {
                                                path.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
                                            }
                                            else if (selectingTiles[selectingTiles.Count - 2].transform.position.y == selectingTiles[selectingTiles.Count - 1].transform.position.y)
                                            {

                                            }
                                            else if ((selectingTiles[selectingTiles.Count - 2].transform.position.y > selectingTiles[selectingTiles.Count - 1].transform.position.y && selectingTiles[selectingTiles.Count - 2].transform.position.x > selectingTiles[selectingTiles.Count - 1].transform.position.x) ||
                                                        (selectingTiles[selectingTiles.Count - 2].transform.position.y < selectingTiles[selectingTiles.Count - 1].transform.position.y && selectingTiles[selectingTiles.Count - 2].transform.position.x < selectingTiles[selectingTiles.Count - 1].transform.position.x))
                                            {
                                                path.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 45));
                                            }
                                            else
                                            {
                                                path.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -45));
                                            }
                                            selectionPaths.Add(path);
                                        }

                                        previousselectingTile = checkingTile;

                                        characterPreview.GetComponent<CreaturePreview>().UpdateStatus(selectingTiles);

                                        break;
                                    }
                                }
                            }
                            //Draw back selection
                            else if (selectingTiles.Count > 1 && selectingTiles.Count < 5)
                            {
                                if (checkingTile == selectingTiles[selectingTiles.Count - 2])
                                {
                                    selectingTiles[selectingTiles.Count - 1].GetComponent<SpriteRenderer>().color = Color.white;
                                    selectingTiles.RemoveAt(selectingTiles.Count - 1);

                                    if (selectionNodes.Count >= 1)
                                    {
                                        Destroy(selectionNodes[selectionNodes.Count - 1]);
                                        selectionNodes.RemoveAt(selectionNodes.Count - 1);
                                    }

                                    if (selectionPaths.Count >= 1)
                                    {
                                        Destroy(selectionPaths[selectionPaths.Count - 1]);
                                        selectionPaths.RemoveAt(selectionPaths.Count - 1);
                                    }


                                    characterPreview.GetComponent<CreaturePreview>().UpdateStatus(selectingTiles);

                                    if (selectingTiles.Count == 0)
                                        previousselectingTile = null;
                                    else
                                        previousselectingTile = selectingTiles[selectingTiles.Count - 1];
                                }
                            }

                            characterPreview.GetComponent<CreaturePreview>().UpdateNormalColor();
                        }
                    }
                }
            }
        }
        ////
    }

    internal void CheckEmtyCenterLanes()
    {
        if (gridLanes[1].Count == 0)
        {
            foreach (PartTile tile in gridLanes[0])
            {
                tile.Lance = gridLanes[1];
                gridLanes[1].Add(tile);
            }
            gridLanes[0].Clear();
        }

        if (gridLanes[2].Count == 0)
        {
            foreach (PartTile tile in gridLanes[3])
            {
                tile.Lance = gridLanes[2];
                gridLanes[2].Add(tile);
            }
            gridLanes[3].Clear();
        }
        foreach (Lane lane in gridLanes)
        {
            //while (lane.Count < 8)
            //{
            //    GameObject newTileObj = (GameObject)Instantiate(partTilePrefab, transform.position, Quaternion.identity);
            //    float r = Random.Range(0f, 1f);
            //    newTileObj.GetComponent<PartTile>().Type = r < 0.7f ? (r < 0.3f ? PartTile.PART_TYPE.HEAD : PartTile.PART_TYPE.ARM) : PartTile.PART_TYPE.LEG;
            //    lane.Add(newTileObj.GetComponent<PartTile>());
            //    newTileObj.GetComponent<PartTile>().SetUpInitPoisition(lane, 10);
            //}
            lane.Refresh();
        }
    }

    internal void SpawnCreature(int laneId, DataController.CREATURE_SIDE side, int headCount, bool[] armData, bool[] legData, int armCount, int legCount, int shieldCount, int swordCount, int starCount)
    {
        battleLanes[laneId].SpawnCreature(side, headCount, armData, legData, armCount, legCount, shieldCount, swordCount, starCount);
    }

    internal void EndGame(DataController.CREATURE_SIDE loseSide)
    {
        GetComponent<UIController>().ShowEndGamePanel(loseSide);
        Time.timeScale = 0f;
    }

    public void RestartGame()
    {
        Time.timeScale = 1f;
        Application.LoadLevel("Main");
    }

    internal void PushObstacles()
    {
        if (gridLanes[0].Count != 0 || gridLanes[1].Count != 0 || gridLanes[2].Count != 0 || gridLanes[3].Count != 0)
        {
            for (int i = 0; i < DataController.instance.NUMBER_OF_OBSTACLE_PER_KILL; i++)
            {
                GameObject newTileObj = (GameObject)Instantiate(partTilePrefab, Vector2.zero, Quaternion.identity);
                newTileObj.GetComponent<PartTile>().Type = PartTile.PART_TYPE.OBSTACLE;
                int obstacledLane = Random.Range(0, 4);
                while (gridLanes[obstacledLane].Count == 0)
                {
                    obstacledLane = Random.Range(0, 4);
                }
                gridLanes[obstacledLane].Insert(Random.Range(0, Mathf.Clamp(gridLanes[obstacledLane].Count - 1, 0, 6)), newTileObj.GetComponent<PartTile>());
                newTileObj.GetComponent<PartTile>().SetUpInitPoisition(gridLanes[obstacledLane], gridLanes[obstacledLane].IndexOf(newTileObj.GetComponent<PartTile>()), false);
            }

            for (int i = 0; i < 4; i++)
            {
                gridLanes[i].Refresh();
            }

            ResetSelection();
        }
    }

    internal void ResetSelection()
    {
        foreach (PartTile checkingTile in selectingTiles)
        {
            checkingTile.GetComponent<SpriteRenderer>().color = Color.white;
        }
        foreach (GameObject node in selectionNodes)
        {
            Destroy(node);
        }
        selectionNodes.Clear();
        foreach (GameObject path in selectionPaths)
        {
            Destroy(path);
        }
        selectionPaths.Clear();
        selectingTiles.Clear();
        isSelecting = false;
        Destroy(characterPreview);
    }

    public void CloseGame()
    {
        Application.LoadLevel("Menu");
    }
}
