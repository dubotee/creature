﻿using UnityEngine;
using System.Collections;

public class Base {
    DataController.CREATURE_SIDE _side;

    float _HP;
    float _yPos;

    public float HP
    {
        get { return _HP; }
    }

    public float YPos
    {
        get { return _yPos; }
    }

    public DataController.CREATURE_SIDE Side
    {
        get { return _side; }
    }

    public Base (DataController.CREATURE_SIDE side, float yPos)
    {
        _side = side;
        _HP = DataController.instance.BASE_HP;
        _yPos = yPos;
    }

    public void Hit(float attackBase)
    {
        _HP -= attackBase;
        if (_HP <= 0)
        {
            DataController.instance.GameController.EndGame(_side);
        }

    }

}
