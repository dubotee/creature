﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(CreatureData))]
public class CreaturePreview : MonoBehaviour
{
    public GameObject[] heads;
    public GameObject[] arms;
    public GameObject[] legs;
    public GameObject previewArrow;
    public GameObject aura;

    public Sprite auraAttackSprite;
    public Sprite auraDefenceSprite;
    public Sprite auraPerfectSprite;

    bool[] armSlot;
    bool[] legSlot;

    int armSlotCount;
    int legSlotCount;

    public bool[] ArmSlot
    {
        get { return armSlot; }
    }

    public bool[] LegSlot
    {
        get { return legSlot; }
    }

    Color highlightColor = new Color(1, 1, 1, 0.5f);
    Color normalColor = new Color(1, 1, 1, 1);

    // Use this for initialization
    void Awake()
    {
        armSlot = new bool[7];
        legSlot = new bool[7];
        armSlotCount = 0;
        legSlotCount = 0;

        foreach (GameObject head in heads)
        {
            head.SetActive(false);
        }
        foreach (GameObject arm in arms)
        {
            arm.SetActive(false);
        }
        foreach (GameObject leg in legs)
        {
            leg.SetActive(false);
        }
    }
    void Start()
    {
        //Hightlight();
    }


    private void UpdateAura()
    {
        if (aura != null)
        {
            aura.transform.localScale *= DataController.instance.TEAM_BONUS_RADIUS;
            aura.GetComponent<SpriteRenderer>().sprite = GetComponent<CreatureData>().Focus == DataController.MONSTER_FOCUS.ATTACK ? auraAttackSprite : (GetComponent<CreatureData>().Focus == DataController.MONSTER_FOCUS.DEFEND ? auraDefenceSprite : auraPerfectSprite);
            aura.SetActive(GetComponent<CreatureData>().Focus != DataController.MONSTER_FOCUS.WHITE);
        }
    }

    public void UpdateNormalColor()
    {
        normalColor = GetComponent<CreatureData>().Focus == DataController.MONSTER_FOCUS.PERFECT ? new Color(0.55f, 0, 0.55f) : (GetComponent<CreatureData>().Focus == DataController.MONSTER_FOCUS.ATTACK ? Color.red : (GetComponent<CreatureData>().Focus == DataController.MONSTER_FOCUS.DEFEND ? Color.green : Color.white));

        if (GetComponent<CreatureController>() != null)
        {
            if (GetComponent<CreatureData>().Side == DataController.CREATURE_SIDE.OWN)
            {
                normalColor.r += 0.3f;
                normalColor.g += 0.3f;
                normalColor.b += 0.3f;
            }
            else
            {
                normalColor.r -= 0.2f;
                normalColor.g -= 0.2f;
                normalColor.b -= 0.2f;
            }
        }

        normalColor = GetComponent<CreatureData>().isCompleted() ? normalColor : highlightColor;
        if (previewArrow != null)
        {
            previewArrow.SetActive(GetComponent<CreatureData>().isCompleted());
        }

        this.GetComponent<SpriteRenderer>().color = normalColor;
        foreach (GameObject head in heads)
        {
            head.GetComponent<SpriteRenderer>().color = normalColor;
        }
        foreach (GameObject arm in arms)
        {
            arm.GetComponent<SpriteRenderer>().color = normalColor;
        }
        foreach (GameObject leg in legs)
        {
            leg.GetComponent<SpriteRenderer>().color = normalColor;
        }
    }

    public void UpdateStatus(List<PartTile> partList)
    {
        gameObject.SetActive(partList.Count > 0);
        GetComponent<CreatureData>().HeadCount = 0;
        GetComponent<CreatureData>().ArmCount = 0;
        GetComponent<CreatureData>().LegCount = 0;
        GetComponent<CreatureData>().ShieldCount = 0;
        GetComponent<CreatureData>().SwordCount = 0;
        GetComponent<CreatureData>().StarCount = 0;
        foreach (PartTile tile in partList)
        {
            switch (tile.Type)
            {
                case PartTile.PART_TYPE.HEAD: GetComponent<CreatureData>().HeadCount++;
                    break;
                case PartTile.PART_TYPE.ARM: GetComponent<CreatureData>().ArmCount++;
                    break;
                case PartTile.PART_TYPE.LEG: GetComponent<CreatureData>().LegCount++;
                    break;
                case PartTile.PART_TYPE.SHIELD: GetComponent<CreatureData>().ShieldCount++;
                    break;
                case PartTile.PART_TYPE.SWORD: GetComponent<CreatureData>().SwordCount++;
                    break;
                case PartTile.PART_TYPE.STAR: GetComponent<CreatureData>().StarCount++;
                    break;
            }
        }

        for (int i = 0; i < heads.Length; i++)
        {
            heads[i].SetActive(i < GetComponent<CreatureData>().HeadCount);
        }

        if (armSlotCount + legSlotCount > GetComponent<CreatureData>().ArmCount + GetComponent<CreatureData>().LegCount)
        {
            while (armSlotCount + legSlotCount > GetComponent<CreatureData>().ArmCount + GetComponent<CreatureData>().LegCount)
            {
                int r = Random.Range(0, 7);
                if (armSlot[r] && armSlotCount > GetComponent<CreatureData>().ArmCount)
                {
                    armSlotCount--;
                    armSlot[r] = false;
                    arms[r].SetActive(false);
                }
                else if (legSlot[r] && legSlotCount > GetComponent<CreatureData>().LegCount)
                {
                    legSlotCount--;
                    legSlot[r] = false;
                    legs[r].SetActive(false);
                }
            }
        }
        else if (armSlotCount + legSlotCount < GetComponent<CreatureData>().ArmCount + GetComponent<CreatureData>().LegCount)
        {
            while (armSlotCount + legSlotCount < GetComponent<CreatureData>().ArmCount + GetComponent<CreatureData>().LegCount)
            {
                int r = Random.Range(0, 7);
                if (!armSlot[r] && !legSlot[r])
                {
                    if (armSlotCount < GetComponent<CreatureData>().ArmCount)
                    {
                        armSlotCount++;
                        armSlot[r] = true;
                        arms[r].SetActive(true);
                    }
                    else if (legSlotCount < GetComponent<CreatureData>().LegCount)
                    {
                        legSlotCount++;
                        legSlot[r] = true;
                        legs[r].SetActive(true);
                    }
                }
            }
        }
        GetComponent<CreatureData>().UpdateFocus();
        UpdateAura();
        UpdateNormalColor();
    }

    public void UpdateStatus(int headCount, bool[] armData, bool[] legData)
    {
        armSlot = armData;
        legSlot = legData;

        if (GetComponent<CreatureData>().isPerfect())
        {
            Debug.Log("Perfect");
            armSlot = new bool[7] { true, false, false, false, false, false, true };
            legSlot = new bool[7] { false, false, true, false, true, false, false };
        }

        for (int i = 0; i < heads.Length; i++)
        {
            heads[i].SetActive(i < headCount);
        }
        for (int i = 0; i < armData.Length; i++)
        {
            arms[i].SetActive(armSlot[i]);
        }
        for (int i = 0; i < legData.Length; i++)
        {
            legs[i].SetActive(legSlot[i]);
        }

        UpdateAura();
        UpdateNormalColor();
    }
}
