﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DamageText : MonoBehaviour {
    DataController.CREATURE_SIDE side;

    public DataController.CREATURE_SIDE Side
    {
        get { return side; }
        set {
            GetComponent<Text>().color = value == DataController.CREATURE_SIDE.ENEMY ? Color.green : Color.red;
            side = value; 
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
    }	

    internal void BeginAnim()
    {
        iTween.MoveFrom(gameObject, iTween.Hash("position", transform.position + new Vector3(0.05f, -0.2f, 0f), "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "Remove"));
    }

    void Remove()
    {
        Destroy(this.gameObject);
    }
}
