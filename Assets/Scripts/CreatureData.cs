﻿using UnityEngine;
using System.Collections;

public class CreatureData : MonoBehaviour {
    int headCount;
    int armCount;
    int legCount;
    int swordCount;
    int shieldCount;
    int starCount;
    DataController.CREATURE_SIDE _side;
    DataController.MONSTER_FOCUS _focus = DataController.MONSTER_FOCUS.WHITE;

    public int HeadCount
    {
        set { headCount = value; }
        get { return headCount; }
    }

    public int ArmCount
    {
        get { return armCount; }
        set { armCount = value; }
    }

    public int LegCount
    {
        get { return legCount; }
        set { legCount = value; }
    }

    public int ShieldCount
    {
        get { return shieldCount; }
        set { shieldCount = value; }
    }

    public int SwordCount
    {
        get { return swordCount; }
        set { swordCount = value; }
    }

    public int StarCount
    {
        get { return starCount; }
        set { starCount = value; }
    }

    public DataController.CREATURE_SIDE Side
    {
        get { return _side; }
        set { _side = value; }
    }

    public DataController.MONSTER_FOCUS Focus
    {
        get { return _focus; }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void InitData(DataController.CREATURE_SIDE side, int headCount, int armCount, int legCount, int shieldCount, int swordCount, int starCount)
    {
        this._side = side;
        this.headCount = headCount;
        this.armCount = armCount;
        this.legCount = legCount;
        this.shieldCount = shieldCount;
        this.swordCount = swordCount;
        this.starCount = starCount;
        UpdateFocus();
    }

    public bool isCompleted()
    {
        return armCount + legCount + headCount + starCount + shieldCount + swordCount == 5;
    }

    public bool isPerfect()
    {
        return armCount == 2 && legCount == 2 && headCount == 1;
    }

    public float GetBaseAttack()
    {
        return (DataController.instance.ARM_ATTACK * armCount + DataController.instance.LEG_ATTACK * legCount + DataController.instance.TORSO_ATTACK) * (1f + DataController.instance.HEAD_ATTACK_BONUS * headCount) * (1f + DataController.instance.BONUS_SWORD_RATE * swordCount) * (isPerfect() ? 1f + DataController.instance.PERFECT_ATTACK_BONUS : 1f);
    }

    public float GetBaseSpeed()
    {
        return (DataController.instance.ARM_SPEED * armCount + DataController.instance.LEG_SPEED * legCount + DataController.instance.TORSO_SPEED) * (1f + DataController.instance.HEAD_SPEED_BONUS * headCount) * (isPerfect() ? 1f + DataController.instance.PERFECT_SPEED_BONUS : 1f);
    }

    public float GetBaseAttackDelay()
    {
        return (DataController.instance.ARM_DELAY * armCount + DataController.instance.LEG_DELAY * legCount + DataController.instance.TORSO_DELAY) * (1f - DataController.instance.HEAD_DELAY_BONUS * headCount) * (isPerfect() ? (1f - DataController.instance.PERFECT_DELAY_BONUS) : 1f);
    }

    public float GetBaseArmor()
    {
        return (DataController.instance.ARM_ARMOR * armCount + DataController.instance.LEG_ARMOR * legCount + DataController.instance.TORSO_ARMOR) * (1f + DataController.instance.HEAD_ARMOR_BONUS * headCount) * (1f + DataController.instance.BONUS_SHIELD_RATE * shieldCount) * (isPerfect() ? 1f + DataController.instance.PERFECT_ARMOR_BONUS : 1f);
    }

    internal void UpdateFocus()
    {
        if (isPerfect())
        {
            _focus = DataController.MONSTER_FOCUS.PERFECT;
        }
        else if (GetComponent<CreatureData>().ArmCount >= 3)
        {
            _focus = DataController.MONSTER_FOCUS.ATTACK;
        }
        else if (GetComponent<CreatureData>().LegCount >= 3)
        {
            _focus = DataController.MONSTER_FOCUS.DEFEND;
        }
        else
        {
            _focus = DataController.MONSTER_FOCUS.WHITE;
        }
    }
}
