﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PerfectText : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Text>().text = "Perfect! +" + (int)(DataController.instance.PERFECT_ATTACK_BONUS * 100) + "%";
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    internal void BeginAnim()
    {
        iTween.MoveFrom(gameObject, iTween.Hash("position", transform.position + new Vector3(0.05f, -0.2f, 0f), "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "Remove"));
    }

    void Remove()
    {
        Destroy(this.gameObject);
    }
}
