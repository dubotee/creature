﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CreatureData))]
public class CreatureController : MonoBehaviour
{
    CreaturePreview _creatureView;
    BattleLane _lane;
    int _subLane;

    int _yDir;
    Base _opponentBase;
    List<GameObject> _opponents;
    List<GameObject> _allies;
    float _cooldown = 0f;
    GameObject _target = null;
    DataController.TARGET_TYPE _targetType = DataController.TARGET_TYPE.NONE;
    List<BodyPart> _parts;
    TeamBonusText _teambonusText;

    float _attPower = 0f;
    float _attDelay = 0f;
    float _armor = 0f;
    float _speed = 0f;
    bool _isDead = false;

    int teamAttackBonusCummalation = 0;
    int teamDefendBonusCummalation = 0;
    bool isAttTeamBonus = false;
    bool isDefTeamBonus = false;
    float teamAttackBonusInterval = 0f;
    float teamDefendBonusInterval = 0f;

    public DataController.TARGET_TYPE TargetType
    {
        get { return _targetType; }
    }

    public bool IsDead
    {
        get { return _isDead; }
    }

    public BattleLane Lane
    {
        get { return _lane; }
        set { _lane = value; }
    }

    public int SubLane
    {
        get { return _subLane; }
        set { _subLane = value; }
    }

    public int TeamAttackBonusCummalation
    {
        get { return teamAttackBonusCummalation; }
    }

    public int TeamDefendBonusCummalation
    {
        get { return teamDefendBonusCummalation; }
    }

    // Use this for initialization
    void Awake()
    {
        _creatureView = this.GetComponent<CreaturePreview>();
        _parts = new List<BodyPart>();
    }

    void Start()
    {
        _yDir = (GetComponent<CreatureData>().Side == DataController.CREATURE_SIDE.OWN) ? 1 : -1;
        _opponentBase = (GetComponent<CreatureData>().Side == DataController.CREATURE_SIDE.OWN) ? _lane.EnemyBase : _lane.OwnBase;
        _opponents = (GetComponent<CreatureData>().Side == DataController.CREATURE_SIDE.OWN) ? _lane.Enemies : _lane.Creatures;
        _allies = (GetComponent<CreatureData>().Side == DataController.CREATURE_SIDE.OWN) ? _lane.Creatures : _lane.Enemies;

        GameObject bonusTextObj = (GameObject)Instantiate(DataController.instance.GameController.GetComponent<UIController>().teamBonusTextPrefab, new Vector3(this.transform.position.x, this.transform.position.y, -50), Quaternion.identity);
        _teambonusText = bonusTextObj.GetComponent<TeamBonusText>();
        _teambonusText.transform.parent = DataController.instance.GameController.GetComponent<UIController>().damageCanvas.transform;
        _teambonusText.transform.localScale = new Vector3(1, 1, 1);
        _teambonusText.GetComponent<TeamBonusText>().Host = GetComponent<CreatureData>();

    }

    // Update is called once per frame
    void LateUpdate()
    {
        bool hasAttackAllyInRange = false;
        bool hasDefendAllyInRange = false;
        foreach (GameObject ally in _allies)
        {
            if (ally != gameObject && Lane == ally.GetComponent<CreatureController>().Lane && ally.GetComponent<CreatureData>().Focus != DataController.MONSTER_FOCUS.WHITE)
            {
                //Debug.Log((ally.transform.position - this.transform.position).magnitude);
                if ((ally.transform.position - this.transform.position).magnitude <= DataController.instance.TEAM_BONUS_RADIUS && (ally.GetComponent<CreatureData>().Focus == DataController.MONSTER_FOCUS.ATTACK || ally.GetComponent<CreatureData>().Focus == DataController.MONSTER_FOCUS.PERFECT))
                {
                    hasAttackAllyInRange = true;
                }
                if ((ally.transform.position - this.transform.position).magnitude <= DataController.instance.TEAM_BONUS_RADIUS && (ally.GetComponent<CreatureData>().Focus == DataController.MONSTER_FOCUS.DEFEND || ally.GetComponent<CreatureData>().Focus == DataController.MONSTER_FOCUS.PERFECT))
                {
                    hasDefendAllyInRange = true;
                }
            }
        }
        //Att attibute
        if (hasAttackAllyInRange)
        {
            if (!isAttTeamBonus)
            {
                isAttTeamBonus = true;
                teamAttackBonusCummalation = Mathf.Clamp(1, 0, DataController.instance.TEAM_BONUS_CAP);
                teamAttackBonusInterval = DataController.instance.TEAM_BONUS_INTERVAL;
                UpdateParameters();
            }
            else
            {
                teamAttackBonusInterval -= Time.deltaTime;
                if (teamAttackBonusInterval <= 0f)
                {
                    teamAttackBonusCummalation = Mathf.Clamp(teamAttackBonusCummalation + 1, 0, DataController.instance.TEAM_BONUS_CAP);
                    teamAttackBonusInterval = DataController.instance.TEAM_BONUS_INTERVAL;
                }
                UpdateParameters();
            }
        }
        else
        {
            if (isAttTeamBonus)
            {
                isAttTeamBonus = false;
                teamAttackBonusCummalation = 0;
                teamAttackBonusInterval = 0f;
                UpdateParameters();
            }
        }

        //Def attibute
        if (hasDefendAllyInRange)
        {
            if (!isDefTeamBonus)
            {
                isDefTeamBonus = true;
                teamDefendBonusCummalation = Mathf.Clamp(1, 0, DataController.instance.TEAM_BONUS_CAP);
                teamDefendBonusInterval = DataController.instance.TEAM_BONUS_INTERVAL;
                UpdateParameters();
            }
            else
            {
                teamDefendBonusInterval -= Time.deltaTime;
                if (teamDefendBonusInterval <= 0f)
                {
                    teamDefendBonusCummalation = Mathf.Clamp(teamDefendBonusCummalation + 1, 0, DataController.instance.TEAM_BONUS_CAP);
                    teamDefendBonusInterval = DataController.instance.TEAM_BONUS_INTERVAL;
                }
                UpdateParameters();
            }
        }
        else
        {
            if (isDefTeamBonus)
            {
                isDefTeamBonus = false;
                teamDefendBonusCummalation = 0;
                teamDefendBonusInterval = 0f;
                UpdateParameters();
            }
        }
        _teambonusText.gameObject.SetActive(isAttTeamBonus || isDefTeamBonus);

        if (_targetType == DataController.TARGET_TYPE.CREATURE && _target != null)
        {
            if (GetComponent<CreatureController>().IsDead)
            {
                _target = null;
            }
            else
            {
                _cooldown -= Time.deltaTime * 10f;
                if (_cooldown <= 0f)
                {
                    Attack();
                    _cooldown = _attDelay;
                }
                return;
            }
        }
        else if (_targetType == DataController.TARGET_TYPE.BASE)
        {
        }

        _targetType = DataController.TARGET_TYPE.NONE;
        bool isStucked = false;

        //foreach (GameObject creature in _allies)
        //{
        //    if (creature != this.gameObject && (creature.transform.position.y - transform.position.y) * _yDir < 0.5f && (creature.transform.position.y - transform.position.y) * _yDir > 0f)
        //    {
        //        isStucked = true;
        //        break;
        //    }
        //}
        if (!isStucked)
        {
            foreach (GameObject creature in _opponents)
            {
                if (creature != this.gameObject && (creature.transform.position.y - transform.position.y) * _yDir < 0.5f && (creature.transform.position.y - transform.position.y) * _yDir > 0f)
                {
                    isStucked = true;
                    _target = creature;
                    _targetType = DataController.TARGET_TYPE.CREATURE;
                    _cooldown = _attDelay;
                    break;
                }
            }
            if (!isStucked)
            {
                if ((_opponentBase.YPos - transform.position.y) * _yDir < 0.5f)
                {
                    isStucked = true;
                    _targetType = DataController.TARGET_TYPE.BASE;

                    _cooldown -= Time.deltaTime * 10f;
                    if (_cooldown <= 0f)
                    {
                        AttackBase();
                        _cooldown = _attDelay;
                    }
                }
            }
        }

        if (!isStucked)
        {
            transform.position += new Vector3(0, Time.deltaTime * _yDir * _speed / 10f, 0f);
        }
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y);
    }

    internal void SetCreatureData(DataController.CREATURE_SIDE side, int headCount, bool[] armData, bool[] legData, int subLane, int shieldCount, int swordCount, int starCount)
    {
        GetComponent<CreatureData>().InitData(side, headCount, GetLengthFromLimbData(armData), GetLengthFromLimbData(legData), shieldCount, swordCount, starCount);
        _creatureView.UpdateStatus(headCount, armData, legData);

        _subLane = subLane;

        _parts.Add(new BodyPart(DataController.BODY_PART_TYPE.TORSO, gameObject, this));
        for (int i = 0; i < headCount; i++)
        {
            _parts.Add(new BodyPart(DataController.BODY_PART_TYPE.HEAD, GetComponent<CreaturePreview>().heads[i], this));
        }
        for (int i = 0; i < armData.Length; i++)
        {
            if (armData[i])
            {
                GameObject assigningArm = null;
                foreach (GameObject arm in GetComponent<CreaturePreview>().arms)
                {
                    bool isArmAssigned = false;
                    foreach (BodyPart part in _parts)
                    {
                        if (part.Sprite == arm)
                        {
                            isArmAssigned = true;
                            break;
                        }
                    }
                    if (!isArmAssigned && arm.activeSelf)
                    {
                        assigningArm = arm;
                    }
                }

                if (assigningArm != null)
                {
                    _parts.Add(new BodyPart(DataController.BODY_PART_TYPE.ARM, assigningArm, this));
                }
            }
        }
        for (int i = 0; i < legData.Length; i++)
        {
            if (legData[i])
            {
                GameObject assigningLeg = null;
                foreach (GameObject leg in GetComponent<CreaturePreview>().legs)
                {
                    bool isLegAssigned = false;
                    foreach (BodyPart part in _parts)
                    {
                        if (part.Sprite == leg)
                        {
                            isLegAssigned = true;
                            break;
                        }
                    }
                    if (!isLegAssigned && leg.activeSelf)
                    {
                        assigningLeg = leg;
                    }
                }

                if (assigningLeg != null)
                {
                    _parts.Add(new BodyPart(DataController.BODY_PART_TYPE.LEG, assigningLeg, this));
                }
            }
        }
        UpdateParameters();
    }

    public void UpdateParameters()
    {
        _attPower = GetAttackPower();
        _speed = GetSpeed();
        _attDelay = GetAttackDelay();
        _armor = GetArmor();
    }

    public float GetAttackPower()
    {
        return GetComponent<CreatureData>().GetBaseAttack() + (teamAttackBonusCummalation * DataController.instance.TEAM_ATTACK_BONUS_OVER_TIME * 6);
    }

    public float GetSpeed()
    {
        return GetComponent<CreatureData>().GetBaseSpeed();// +(teamBonusCummalation * DataController.instance.TEAM_SPEED_BONUS_OVER_TIME);
    }

    public float GetAttackDelay()
    {
        return GetComponent<CreatureData>().GetBaseAttackDelay();// +(teamBonusCummalation * DataController.instance.TEAM_DELAY_BONUS_OVER_TIME);
    }

    public float GetArmor()
    {
        return GetComponent<CreatureData>().GetBaseArmor() + (teamDefendBonusCummalation * DataController.instance.TEAM_ARMOR_BONUS_OVER_TIME * 6);
    }

    private void Attack()
    {
        _target.GetComponent<CreatureController>().Hit(_attPower);
    }

    private void AttackBase()
    {
        _opponentBase.Hit(_attPower);
        DataController.instance.GameController.GetComponent<UIController>().ShowDamageText(new Vector3(transform.position.x, _opponentBase.YPos), (int)_attPower, _opponentBase.Side);
    }

    private void Hit(float attackPower)
    {
        BodyPart hitPart = RandomPartTarget();
        hitPart.HP -= Mathf.Clamp(attackPower - _armor, 8f, float.MaxValue);

        DataController.instance.GameController.GetComponent<UIController>().ShowDamageText(hitPart.Sprite.gameObject.transform.position, (int)Mathf.Clamp(attackPower - _armor, 8f, float.MaxValue), GetComponent<CreatureData>().Side);
    }

    public BodyPart RandomPartTarget()
    {
        int torsoRate = GetComponent<CreatureData>().LegCount > 0 ? 2 : 40;
        int headRate = GetComponent<CreatureData>().HeadCount > 0 ? (GetComponent<CreatureData>().ArmCount > 0 ? 12 : 60) : 0;
        int armRate = GetComponent<CreatureData>().ArmCount > 0 ? (GetComponent<CreatureData>().HeadCount > 0 ? 42 : 60) : 0;
        int legRate = GetComponent<CreatureData>().LegCount > 0 ? 38 : 0;

        int r = Random.Range(0, torsoRate + headRate + armRate + legRate);
        if (r < torsoRate)
        {
            for (int i = _parts.Count - 1; i >= 0; i--)
            {
                if (_parts[i].Type == DataController.BODY_PART_TYPE.TORSO) return _parts[i];
            }
        }
        else if (r < headRate + torsoRate)
        {
            for (int i = _parts.Count - 1; i >= 0; i--)
            {
                if (_parts[i].Type == DataController.BODY_PART_TYPE.HEAD) return _parts[i];
            }
        }
        else if (r < headRate + torsoRate + armRate)
        {
            for (int i = _parts.Count - 1; i >= 0; i--)
            {
                if (_parts[i].Type == DataController.BODY_PART_TYPE.ARM) return _parts[i];
            }
        }
        else if (r < headRate + torsoRate + armRate + legRate)
        {
            for (int i = _parts.Count - 1; i >= 0; i--)
            {
                if (_parts[i].Type == DataController.BODY_PART_TYPE.LEG) return _parts[i];
            }
        }
        return _parts[_parts.Count - 1];
    }

    internal void RemovePart(BodyPart part)
    {
        part.Sprite.SetActive(false);
        _parts.Remove(part);

        switch (part.Type)
        {
            case DataController.BODY_PART_TYPE.HEAD: GetComponent<CreatureData>().HeadCount--;
                break;
            case DataController.BODY_PART_TYPE.ARM: GetComponent<CreatureData>().ArmCount--;
                break;
            case DataController.BODY_PART_TYPE.LEG: GetComponent<CreatureData>().LegCount--;
                break;
        }

        UpdateParameters();
        if (part.Type == DataController.BODY_PART_TYPE.TORSO && _parts.Count > 1)
        {
            DataController.instance.GameController.GetComponent<UIController>().ShowDamageText(part.Sprite.gameObject.transform.position, 0, GetComponent<CreatureData>().Side, true);
        }

        if (_parts.Count == 0 || part.Type == DataController.BODY_PART_TYPE.TORSO)
        {
            if (GetComponent<CreatureData>().Side == DataController.CREATURE_SIDE.OWN)
            {
                DataController.instance.GameController.PushObstacles();
            }
            Kill();
        }
    }

    internal void Kill()
    {
        _isDead = true;
        _allies.Remove(this.gameObject);
        Destroy(this.gameObject);
    }

    internal int GetLengthFromLimbData(bool[] limbData)
    {
        int count = 0;
        for (int i = 0; i < limbData.Length; i++)
        {
            if (limbData[i]) count++;
        }
        return count;
    }
}
