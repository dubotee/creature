﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(GameController))]
public class UIController : MonoBehaviour
{
    public GameObject damageTextPrefab;
    public GameObject perfectTextPrefab;
    public GameObject teamBonusTextPrefab;
    public GameObject specialTileTextPrefab;
    public GameObject previewInfoTextPrefab;
    public GameObject obstacleElapsedTextPrefab;

    public Text gameResultLabel;
    public GameObject endGamePanel;
    public Canvas damageCanvas;

    public Text ownHeadCountText;
    public Text ownHandCountText;
    public Text ownLegCountText;
    public Text ownSwordCountText;
    public Text ownShieldCountText;

    public Text enemyHeadCountText;
    public Text enemyHandCountText;
    public Text enemyLegCountText;
    public Text enemySwordCountText;
    public Text enemyShieldCountText;

    public Image OwnBaseHP;
    public Image EnemyBaseHP;

    public GameObject enemyBase1;
    public GameObject enemyBase2;
    public GameObject ownBase1;
    public GameObject ownBase2;

    // Use this for initialization
    void Start()
    {
        iTween.ColorTo(enemyBase1, iTween.Hash("color", Color.red, "time", 0.5f, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.loop));
    }

    // Update is called once per frame
    void Update()
    {
        ownHeadCountText.text = "Heads: " + GetComponent<GameController>().PlayerHead + "/" + DataController.instance.TOTAL_MONSTER;
        ownHandCountText.text = "Hands: " + GetComponent<GameController>().PlayerArm + "/" + DataController.instance.TOTAL_MONSTER * 2;
        ownLegCountText.text = "Legs: " + GetComponent<GameController>().PlayerLeg + "/" + DataController.instance.TOTAL_MONSTER * 2;
        ownSwordCountText.text = "Swords: " + GetComponent<GameController>().PlayerSword + "/" + DataController.instance.TOTAL_POWERUP_SWORD;
        ownShieldCountText.text = "Shields: " + GetComponent<GameController>().PlayerShield + "/" + DataController.instance.TOTAL_POWERUP_SHIELD;

        enemyHeadCountText.text = "Heads: " + GetComponent<BotController>().totalHead + "/" + DataController.instance.TOTAL_MONSTER;
        enemyHandCountText.text = "Hands: " + GetComponent<BotController>().totalArm + "/" + DataController.instance.TOTAL_MONSTER * 2;
        enemyLegCountText.text = "Legs: " + GetComponent<BotController>().totalLeg + "/" + DataController.instance.TOTAL_MONSTER * 2;
        enemySwordCountText.text = "Swords: " + GetComponent<BotController>().totalSword + "/" + DataController.instance.TOTAL_POWERUP_SWORD;
        enemyShieldCountText.text = "Shields: " + GetComponent<BotController>().totalShield + "/" + DataController.instance.TOTAL_POWERUP_SHIELD;

        OwnBaseHP.fillAmount = Mathf.Clamp(GetComponent<GameController>().OwnBase.HP, 0f, DataController.instance.BASE_HP) / (float)DataController.instance.BASE_HP;
        EnemyBaseHP.fillAmount = Mathf.Clamp(GetComponent<GameController>().EnemyBase.HP, 0f, DataController.instance.BASE_HP) / (float)DataController.instance.BASE_HP;

        
    }

    public void ShowEndGamePanel(DataController.CREATURE_SIDE loseSide)
    {
        endGamePanel.SetActive(true);
        gameResultLabel.text = loseSide == DataController.CREATURE_SIDE.OWN ? "YOU LOSE!" : "YOU WIN!";
    }


    public void ShowDamageText(Vector2 pos, int damageText, DataController.CREATURE_SIDE side, bool isCritical = false)
    {
        GameObject damageTextObj = (GameObject)Instantiate(damageTextPrefab, new Vector3(pos.x, pos.y, -50), Quaternion.identity);
        damageTextObj.GetComponent<Text>().text = isCritical ? "Critical" : "-" + damageText.ToString();
        damageTextObj.transform.parent = damageCanvas.transform;
        damageTextObj.transform.localScale = new Vector3(1, 1, 1);
        damageTextObj.GetComponent<DamageText>().Side = side;
        damageTextObj.GetComponent<DamageText>().BeginAnim();
    }

    public void ShowPerfectText(Vector2 pos)
    {
        GameObject damageTextObj = (GameObject)Instantiate(perfectTextPrefab, new Vector3(0, 0, -50), Quaternion.identity);
        damageTextObj.transform.parent = damageCanvas.transform;
        damageTextObj.transform.localScale = new Vector3(1, 1, 1);
        damageTextObj.GetComponent<PerfectText>().BeginAnim();
    }

    public void ShowSpecialTileText(Vector2 pos, PartTile.PART_TYPE type)
    {
        GameObject damageTextObj = (GameObject)Instantiate(specialTileTextPrefab, new Vector3(pos.x, pos.y, -50), Quaternion.identity);
        damageTextObj.transform.parent = damageCanvas.transform;
        damageTextObj.transform.localScale = new Vector3(1, 1, 1);
        damageTextObj.GetComponent<SpecialTileText>().Type = type;
        damageTextObj.GetComponent<SpecialTileText>().BeginAnim();
    }

    public void ShowTeamBonusText(Vector2 pos, int bonusLevel, CreatureData host)
    {
        GameObject damageTextObj = (GameObject)Instantiate(teamBonusTextPrefab, new Vector3(pos.x, pos.y, -50), Quaternion.identity);

        string text = "";
        text += DataController.instance.TEAM_ATTACK_BONUS_OVER_TIME > 0 ? ("+" + DataController.instance.TEAM_ATTACK_BONUS_OVER_TIME * 6 + " ATT\n") : "";
        text += DataController.instance.TEAM_ARMOR_BONUS_OVER_TIME > 0 ? ("+" + DataController.instance.TEAM_ARMOR_BONUS_OVER_TIME * 6 + " DEF\n") : "";
        text += DataController.instance.TEAM_SPEED_BONUS_OVER_TIME > 0 ? ("+" + DataController.instance.TEAM_SPEED_BONUS_OVER_TIME * 6 + " SPD\n") : "";
        text += DataController.instance.TEAM_DELAY_BONUS_OVER_TIME > 0 ? ("+" + DataController.instance.TEAM_DELAY_BONUS_OVER_TIME * 6 + " ASPD") : "";

        damageTextObj.GetComponent<Text>().text = text;
        damageTextObj.transform.parent = damageCanvas.transform;
        damageTextObj.transform.localScale = new Vector3(1, 1, 1);
        damageTextObj.GetComponent<TeamBonusText>().Host = host;
    }
}
