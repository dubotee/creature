﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {
    public GameObject MainPanel;
    public GameObject HelpPanel;
	// Use this for initialization
	void Start () {
        MainPanel.gameObject.SetActive(true);
        HelpPanel.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void BackToMain()
    {
        MainPanel.gameObject.SetActive(true);
        HelpPanel.gameObject.SetActive(false);
    }

    public void ToHelpPanel()
    {
        MainPanel.gameObject.SetActive(false);
        HelpPanel.gameObject.SetActive(true);
    }

    public void Play()
    {
        Application.LoadLevel("Main");
    }
}
