﻿using UnityEngine;
using System.Collections;

public class DataController : MonoSingleton<DataController>
{
    public enum CREATURE_SIDE
    {
        OWN,
        ENEMY
    }

    public enum TARGET_TYPE
    {
        NONE = 0,
        BASE,
        CREATURE
    }

    public enum BODY_PART_TYPE
    {
        TORSO,
        HEAD,
        ARM,
        LEG
    }

    public enum MONSTER_FOCUS
    {
        ATTACK,
        DEFEND,
        PERFECT,
        WHITE
    }

    [Header ("UI")]
    public float BATTLE_LANE_RATE = 1.2f;
    public float GRID_LANE_RATE = 1.0f;
    public float LANE_OFFSET = 0.25f;
    public float BASE_Y_OFFSET = 0.8f;

    [Header("GAMEPLAY")]
    public float BASE_HP = 100f;
    public int TOTAL_MONSTER = 10;
    public int TOTAL_POWERUP_SWORD = 3;
    public int TOTAL_POWERUP_SHIELD = 2;
    public int TOTAL_POWERUP_STAR = 0;
    public float TEAM_BONUS_INTERVAL = 1f;
    public float TEAM_BONUS_RADIUS = 0.5f;
    public int TEAM_BONUS_CAP = 15;

    public float BONUS_SWORD_RATE = 0.1f;
    public float BONUS_SHIELD_RATE = 0.1f;

    [Header("PENALTY")]
    public int NUMBER_OF_OBSTACLE_PER_KILL = 2;
    public int OBSTACLE_COLLAPSE_TIME = 4;

    [Header("MONSTER FORMULAS")]
    public float HEAD_SPEED_BONUS = 0.1f;
    public float HEAD_ARMOR_BONUS = 0.1f;
    public float HEAD_ATTACK_BONUS = 0.1f;
    public float HEAD_DELAY_BONUS = 0.1f;
    [Header("")]
    public float PERFECT_SPEED_BONUS = 0.1f;
    public float PERFECT_ARMOR_BONUS = 0.2f;
    public float PERFECT_ATTACK_BONUS = 0.1f;
    public float PERFECT_DELAY_BONUS = 0.15f;
    [Header("")]
    public float TEAM_SPEED_BONUS_OVER_TIME = 0.1f;
    public float TEAM_ARMOR_BONUS_OVER_TIME = 0.2f;
    public float TEAM_ATTACK_BONUS_OVER_TIME = 0.1f;
    public float TEAM_DELAY_BONUS_OVER_TIME = 0.15f;

    [Header("1.Head")]
    [Header("MONSTER DATA")]
    public float HEAD_HP = 10f;
    [Header("2.Torso")]
    public float TORSO_HP = 30f;
    public float TORSO_ATTACK = 7f;
    public float TORSO_DELAY = 7f;
    public float TORSO_ARMOR = 1f;
    public float TORSO_SPEED = 1f;
    [Header("3.Arm")]
    public float ARM_HP = 15f;
    public float ARM_ATTACK = 7f;
    public float ARM_DELAY = 1f;
    public float ARM_ARMOR = 1f;
    public float ARM_SPEED = 1f;
    [Header("4.Leg")]
    public float LEG_HP = 20f;
    public float LEG_ATTACK = 10f;
    public float LEG_DELAY = 3f;
    public float LEG_ARMOR = 4f;
    public float LEG_SPEED = 2f;
    
    GameController _currGameController;
    float _tileSize = 1f;
    float _baseLeftX = -1f;
    float _baseRightX = 1f;
    float _baseEnemyY = 1f;
    float _baseOwnY = -1f;

    public GameController GameController
    {
        get { return _currGameController; }
        set { _currGameController = value; }
    }

    public float TileSize
    {
        get { return _tileSize; }
    }

    public float BaseLeftX
    {
        get { return _baseLeftX; }
    }

    public float BaseRightX
    {
        get { return _baseRightX; }
    }

    public float BaseEnemyY
    {
        get { return _baseEnemyY; }
    }

    public float BaseOwnY
    {
        get { return _baseOwnY; }
    }

    internal void CalculateScreenSize()
    {
        Vector2 screenSizeToWorldSize = Camera.allCameras[0].ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f)) * 2f;
        float relativeSize = screenSizeToWorldSize.x / (GRID_LANE_RATE * 4 + BATTLE_LANE_RATE * 2);
        _tileSize = relativeSize * GRID_LANE_RATE;

        _baseRightX = (screenSizeToWorldSize.x - relativeSize * BATTLE_LANE_RATE) / 2;
        _baseLeftX = -_baseRightX;

        _baseEnemyY = screenSizeToWorldSize.y / 2f * BASE_Y_OFFSET;
        _baseOwnY = -_baseEnemyY;
    }
}
