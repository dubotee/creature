﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LobbyController : Photon.MonoBehaviour
{
    public Text roomName;
    public GameObject loginPanel;
    public GameObject findPanel;
    public GameObject joinPanel;

    public Text roomPlayer_1Text;
    public Text roomPlayer_2Text;
    public Text roomCoundownText;

    public Text findRoom_1Text;
    public Text findRoom_2Text;
    public Text findRoom_3Text;

    public Button findRoom_1Button;
    public Button findRoom_2Button;
    public Button findRoom_3Button;
    private bool isCountDown;
    private float countDownNum;

    // Use this for initialization
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings("0.1");
        loginPanel.SetActive(false);
        findPanel.SetActive(false);
        joinPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        roomCoundownText.text = "";

        roomCoundownText.gameObject.SetActive(isCountDown);
        if (isCountDown)
        {
            countDownNum -= Time.deltaTime;
            roomCoundownText.text = Mathf.Clamp(Mathf.FloorToInt(countDownNum), 0, int.MaxValue).ToString();

            if (countDownNum <= 0 && PhotonNetwork.playerList.Length >= 2)
            {
                PhotonNetwork.LoadLevel("Main");
            }
        }
    }

    void UpdateRoomPlayerName ()
    {
        roomPlayer_1Text.text = PhotonNetwork.playerList.Length >= 1 ? PhotonNetwork.playerList[0].name : "";
        roomPlayer_2Text.text = PhotonNetwork.playerList.Length >= 2 ? PhotonNetwork.playerList[1].name : "";
    }

    public void Host()
    {
        //RoomOptions roomOptions = new RoomOptions() { isVisible = false, maxPlayers = 2 };
        //PhotonNetwork.CreateRoom(roomName.text == "" ? "Default" : roomName.text, roomOptions, TypedLobby.Default);
        PhotonNetwork.CreateRoom(roomName.text == "" ? "Default" : roomName.text);

        PhotonNetwork.player.name = roomName.text == "" ? "Unnamed" : roomName.text;
        loginPanel.SetActive(false);
    }

    public void Find()
    {
        PhotonNetwork.player.name = roomName.text == "" ? "Unnamed" : roomName.text;

        loginPanel.SetActive(false);
        findPanel.SetActive(true);

        RefreshRoomList();
    }

    private void RefreshRoomList()
    {
        findRoom_1Text.gameObject.SetActive(false);
        findRoom_2Text.gameObject.SetActive(false);
        findRoom_3Text.gameObject.SetActive(false);

        findRoom_1Button.gameObject.SetActive(false);
        findRoom_2Button.gameObject.SetActive(false);
        findRoom_3Button.gameObject.SetActive(false);


        if (PhotonNetwork.GetRoomList().Length > 0)
        {
            findRoom_1Text.gameObject.SetActive(true);
            findRoom_1Button.gameObject.SetActive(true);
            findRoom_1Text.text = PhotonNetwork.GetRoomList()[0].name;
        }
        if (PhotonNetwork.GetRoomList().Length > 1)
        {
            findRoom_2Text.gameObject.SetActive(true);
            findRoom_2Button.gameObject.SetActive(true);
            findRoom_2Text.text = PhotonNetwork.GetRoomList()[1].name;
        }
        if (PhotonNetwork.GetRoomList().Length > 2)
        {
            findRoom_3Text.gameObject.SetActive(true);
            findRoom_3Button.gameObject.SetActive(true);
            findRoom_3Text.text = PhotonNetwork.GetRoomList()[2].name;
        }
    }

    public void BackFromFindRoom()
    {
        loginPanel.SetActive(true);
        findPanel.SetActive(false);
    }

    public void BackFromRoom()
    {
        PhotonNetwork.LeaveRoom();
        joinPanel.SetActive(false);

        isCountDown = false;
    }

    public void JoinRoom_1()
    {
        PhotonNetwork.JoinRoom(PhotonNetwork.GetRoomList()[0].name);
        findPanel.SetActive(false);
    }

    public void JoinRoom_2()
    {
        PhotonNetwork.JoinRoom(PhotonNetwork.GetRoomList()[1].name);
        findPanel.SetActive(false);
    }

    public void JoinRoom_3()
    {
        PhotonNetwork.JoinRoom(PhotonNetwork.GetRoomList()[2].name);
        findPanel.SetActive(false);
    }

    public virtual void OnConnectedToPhoton()
    {
        loginPanel.SetActive(true);
    }

    public virtual void OnCreatedRoom()
    {
        joinPanel.SetActive(true);
        UpdateRoomPlayerName();
    }

    public virtual void OnPhotonCreateRoomFailed()
    {
        loginPanel.SetActive(true);
    }

    public virtual void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
    }

    public virtual void OnPhotonPlayerConnected (PhotonPlayer newPlayer)
    {
        UpdateRoomPlayerName();

        if (PhotonNetwork.playerList.Length >= 2)
        {
            PhotonNetwork.room.visible = false;
            isCountDown = true;
            countDownNum = 4f;
        }
    }

    public virtual void OnPhotonPlayerDisconnected (PhotonPlayer leftPlayer)
    {
        UpdateRoomPlayerName();
        PhotonNetwork.room.visible = true;
        isCountDown = false;
    }

    public virtual void OnJoinedLobby()
    {
        loginPanel.SetActive(true);
    }

    public virtual void OnJoinedRoom ()
    {
        UpdateRoomPlayerName();
        joinPanel.SetActive(true);

        if (PhotonNetwork.playerList.Length >= 2)
        {
            PhotonNetwork.room.visible = false;
            isCountDown = true;
            countDownNum = 4f;
        }
    }

    public virtual void OnReceivedRoomListUpdate  (  )
    {
        if (findPanel.gameObject.activeSelf)
        {
            RefreshRoomList();
        }
    }
}
