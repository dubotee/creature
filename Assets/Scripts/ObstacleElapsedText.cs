﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObstacleElapsedText : MonoBehaviour {
    PartTile _host;

    public PartTile Host
    {
        get { return _host; }
        set
        {
            _host = value;
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (_host != null)
        {
            transform.position = _host.transform.position + new Vector3(0, 0, -50);
            GetComponent<Text>().text = Mathf.Clamp(Mathf.CeilToInt(_host.CollapsedTime), 0, int.MaxValue).ToString();
        }
        else
        {
            Destroy(this.gameObject);
        }
	}
}
