﻿using UnityEngine;
using System.Collections;

public class BotController : MonoBehaviour {
    float nextSpawningTime = float.MaxValue;

    [HideInInspector]
    public int totalHead = 10;
    [HideInInspector]
    public int totalArm = 20;
    [HideInInspector]
    public int totalLeg = 20;
    [HideInInspector]
    public int totalShield = 2;
    [HideInInspector]
    public int totalSword = 2;
    
	// Use this for initialization
	void Start () {
        totalHead = DataController.instance.TOTAL_MONSTER;
        totalArm = DataController.instance.TOTAL_MONSTER * 2;
        totalLeg = DataController.instance.TOTAL_MONSTER * 2;
        totalShield = DataController.instance.TOTAL_POWERUP_SHIELD;
        totalSword = DataController.instance.TOTAL_POWERUP_SWORD;
        nextSpawningTime = Random.Range(1f, 5f);
	}
	
	// Update is called once per frame
    void Update()
    {
        nextSpawningTime -= Time.deltaTime;
        if (nextSpawningTime < 0f && totalArm + totalHead + totalLeg >= 5 && DataController.instance.GameController.BattleLanes[0].Enemies.Count + DataController.instance.GameController.BattleLanes[1].Enemies.Count < 3)
        {
            bool willSpawn = false;
            int spawnLane = 0;

            if (DataController.instance.GameController.BattleLanes[0].Creatures.Count > 0 && DataController.instance.GameController.BattleLanes[0].Creatures.Count > DataController.instance.GameController.BattleLanes[0].Enemies.Count)
            {
                willSpawn = true;
                spawnLane = 0;
            }
            else if (DataController.instance.GameController.BattleLanes[1].Creatures.Count > 0 && DataController.instance.GameController.BattleLanes[1].Creatures.Count > DataController.instance.GameController.BattleLanes[1].Enemies.Count)
            {
                willSpawn = true;
                spawnLane = 1;
            }
            else
            {
                //willSpawn = Random.Range(0, 1000) < 5;
                willSpawn = true;
                spawnLane = Random.Range(0, 2);
            }

            if (willSpawn)
            {
                nextSpawningTime = 1f;

                int headCount = Mathf.Clamp(Random.Range(0, 3), 0, totalHead);
                int armCount = Mathf.Clamp(Random.Range(0, 6 - headCount), 0, totalArm);
                int shieldCount = Random.Range(0, 5) < 4 ? 0 : Mathf.Clamp(Random.Range(1, totalShield + 1), 0, totalShield);
                int swordCount = Random.Range(0, 5) < 4 ? 0 : Mathf.Clamp(Random.Range(1, totalSword + 1), 0, totalSword);
                int legCount = Mathf.Clamp(5 - headCount - armCount - shieldCount - swordCount, 0, totalLeg);

                if (totalArm + totalHead + totalLeg + totalSword + totalShield == 5)
                {
                    headCount = totalHead;
                    armCount = totalArm;
                    legCount = totalLeg;
                    shieldCount = totalShield;
                    swordCount = totalSword;
                }

                if (headCount + armCount + shieldCount + swordCount + legCount == 5)
                {
                    bool[] armData = new bool[7];
                    bool[] legData = new bool[7];

                    for (int i = 0; i < armCount; i++)
                    {
                        int randomLimbsSlot = 0;
                        do
                        {
                            randomLimbsSlot = Random.Range(0, 7);
                        }
                        while (armData[randomLimbsSlot] || legData[randomLimbsSlot]);
                        armData[randomLimbsSlot] = true;
                    }
                    for (int i = 0; i < legCount; i++)
                    {
                        int randomLimbsSlot = 0;
                        do
                        {
                            randomLimbsSlot = Random.Range(0, 7);
                        }
                        while (armData[randomLimbsSlot] || legData[randomLimbsSlot]);
                        legData[randomLimbsSlot] = true;
                    }

                    totalHead -= headCount;
                    totalArm -= armCount;
                    totalLeg -= legCount;
                    totalShield -= shieldCount;
                    totalSword -= swordCount;

                    DataController.instance.GameController.SpawnCreature(spawnLane, DataController.CREATURE_SIDE.ENEMY, headCount, armData, legData, armCount, legCount, shieldCount, swordCount, 0);
                }
            }
        }
	}
}
