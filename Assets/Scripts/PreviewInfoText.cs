﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PreviewInfoText : MonoBehaviour {
    GameObject _host;

    public GameObject Host
    {
        get { return _host; }
        set
        {
            _host = value;
        }
    }
	// Use this for initialization
    void Start()
    {
	}
	
	// Update is called once per frame
	void Update () {
        if (_host != null)
        {
            transform.position = _host.transform.position + new Vector3(0f, 1.4f);
            gameObject.SetActive(Host.activeSelf);

            string text = "ATT: " + Mathf.RoundToInt(Host.GetComponent<CreatureData>().GetBaseAttack()) + "\n" + "DEF: " + Mathf.RoundToInt(Host.GetComponent<CreatureData>().GetBaseArmor());
            GetComponent<Text>().text = text;
        }
        else
        {
            Destroy(this.gameObject);
        }
	}
}
