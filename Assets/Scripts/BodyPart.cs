﻿using UnityEngine;
using System.Collections;

public class BodyPart
{

    DataController.BODY_PART_TYPE _type;

    float _HP = 0;
    float _attPower = 0;
    float _attDelay = 0;
    float _armor = 0;
    float _speed = 0;
    CreatureController _host = null;
    GameObject _sprite;

    public DataController.BODY_PART_TYPE Type
    {
        get { return _type; }
    }

    public float HP
    {
        get { return _HP; }
        set
        {
            _HP = value;
            if (_HP <= 0f)
            {
                _host.RemovePart(this);
            }
        }
    }

    public GameObject Sprite
    {
        get { return _sprite; }
    }

    public BodyPart(DataController.BODY_PART_TYPE type, GameObject sprite, CreatureController host)
    {
        _type = type;
        _sprite = sprite;
        _host = host;
        switch (type)
        {
            case DataController.BODY_PART_TYPE.TORSO:
                _HP = DataController.instance.TORSO_HP;
                break;
            case DataController.BODY_PART_TYPE.ARM:
                _HP = DataController.instance.ARM_HP;
                break;
            case DataController.BODY_PART_TYPE.LEG:
                _HP = DataController.instance.LEG_HP;
                break;
            case DataController.BODY_PART_TYPE.HEAD:
                _HP = DataController.instance.HEAD_HP;
                break;
        }
    }
}
