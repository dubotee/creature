﻿using UnityEngine;
using System.Collections;

public class PreviewArrow : MonoBehaviour {
    public GameObject leftArrow;
    public GameObject rightArrow;
	// Use this for initialization
	void Start () {
        iTween.ColorTo(leftArrow, iTween.Hash("color", Color.red, "time", 0.5f, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.loop));
        iTween.ColorTo(rightArrow, iTween.Hash("color", Color.red, "time", 0.5f, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.loop));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
