﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpecialTileText : MonoBehaviour {
    PartTile.PART_TYPE _type;

    public PartTile.PART_TYPE Type
    {
        get { return _type; }
        set { 
            _type = value;
            GetComponent<Text>().color = Type == PartTile.PART_TYPE.SHIELD ? Color.green : Color.red;
            GetComponent<Text>().text = Type == PartTile.PART_TYPE.SHIELD ? "Shield +" + (int)(DataController.instance.BONUS_SHIELD_RATE * 100) + "% DEF" : "Sword +" + (int)(DataController.instance.BONUS_SWORD_RATE * 100) + "% ATT";
        }
    }
	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {

    }

    internal void BeginAnim()
    {
        iTween.MoveFrom(gameObject, iTween.Hash("position", transform.position + new Vector3(0.05f, -0.2f, 0f), "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "Remove"));
    }

    void Remove()
    {
        Destroy(this.gameObject);
    }
}
