﻿using UnityEngine;
using System.Collections;

public class PartTile : MonoBehaviour
{
    public Sprite headSprite;
    public Sprite armSprite;
    public Sprite legSprite;
    public Sprite swordSprite;
    public Sprite shieldSprite;
    public Sprite starSprite;
    public Sprite obstacleSprite;

    public enum PART_TYPE
    {
        LEG,
        ARM,
        HEAD, 
        SHIELD,
        SWORD,
        STAR,
        OBSTACLE
    }

    PART_TYPE _type = PART_TYPE.LEG;
    Lane _lance;
    int pos;
    float _collapsedTime;
    bool isSelected;
    bool isDisabled;

    public float CollapsedTime
    {
        get { return _collapsedTime; }
        set { _collapsedTime = value; }
    }

    public bool isSpecialTile
    {
        get
        {
            return _type == PART_TYPE.SWORD || _type == PART_TYPE.SHIELD || _type == PART_TYPE.STAR;
        }
    }

    public bool IsDisabled
    {
        get
        {
            return isDisabled;
        }
        set
        {
            GetComponent<SpriteRenderer>().color = value ? Color.gray : Color.white;
            isDisabled = value;
        }
    }

    public int Pos
    {
        get { return pos; }
        set { pos = value; }
    }

    public Lane Lance
    {
        get { return _lance; }
        set { _lance = value; }
    }

    public PART_TYPE Type
    {
        get { return _type; }
        set
        {
            switch (value)
            {
                case PART_TYPE.ARM: GetComponent<SpriteRenderer>().sprite = armSprite;
                    break;
                case PART_TYPE.HEAD: GetComponent<SpriteRenderer>().sprite = headSprite;
                    break;
                case PART_TYPE.LEG: GetComponent<SpriteRenderer>().sprite = legSprite;
                    break;
                case PART_TYPE.SHIELD: GetComponent<SpriteRenderer>().sprite = shieldSprite;
                    break;
                case PART_TYPE.SWORD: GetComponent<SpriteRenderer>().sprite = swordSprite;
                    break;
                case PART_TYPE.OBSTACLE: GetComponent<SpriteRenderer>().sprite = obstacleSprite;
                    break;
            }
            _type = value;
        }
    }

    internal void SetUpInitPoisition(Lane currentLance, int queuePos, bool animated = true)
    {
        transform.localScale = Vector2.one * DataController.instance.TileSize;
        float x = DataController.instance.GameController.transform.position.x + DataController.instance.TileSize * currentLance.LanceId - DataController.instance.TileSize * 2 + DataController.instance.TileSize / 2f;
        float y = DataController.instance.GameController.transform.position.y + DataController.instance.TileSize * queuePos - DataController.instance.TileSize * 4 + DataController.instance.TileSize / 2f;

        Pos = queuePos;
        Lance = currentLance;

        transform.position = new Vector3(x, y, 0);

        if (queuePos >= 8)
        {
            gameObject.SetActive(false);
        }
        else
        {
            if (animated)
            {
                iTween.Stop(gameObject);
                iTween.MoveFrom(gameObject, iTween.Hash("position", transform.position + new Vector3(0, 10, 0), "time", 1f + Random.Range(0f, 0.5f) + queuePos * 0.3f, "easetype", iTween.EaseType.linear));
            }
        }

        if (_type == PART_TYPE.OBSTACLE)
        {
            GameObject bonusTextObj = (GameObject)Instantiate(DataController.instance.GameController.GetComponent<UIController>().obstacleElapsedTextPrefab, new Vector3(this.transform.position.x, this.transform.position.y, -50), Quaternion.identity);
            ObstacleElapsedText _teambonusText = bonusTextObj.GetComponent<ObstacleElapsedText>();
            _teambonusText.transform.parent = DataController.instance.GameController.GetComponent<UIController>().damageCanvas.transform;
            _teambonusText.transform.localScale = new Vector3(1, 1, 1);
            _teambonusText.Host = this;

            _collapsedTime = DataController.instance.OBSTACLE_COLLAPSE_TIME;
        }
    }

    internal void UpdateQueuePostition(int queuePos)
    {
        float x = DataController.instance.GameController.transform.position.x + DataController.instance.TileSize * Lance.LanceId - DataController.instance.TileSize * 2 + DataController.instance.TileSize / 2f;
        float y = DataController.instance.GameController.transform.position.y + DataController.instance.TileSize * queuePos - DataController.instance.TileSize * 4 + DataController.instance.TileSize / 2f;

        gameObject.SetActive(queuePos < 8);
        if (Pos >= 8 && queuePos < 8)
        {
            transform.position = new Vector3(x, y, 0);
            gameObject.SetActive(true);
            iTween.Stop(gameObject);
            iTween.MoveFrom(gameObject, iTween.Hash("position", transform.position + new Vector3(0, 4, 0), "time", Random.Range(0f, 0.5f) + queuePos * 0.1f, "easetype", iTween.EaseType.linear));
        }
        else
        {
            iTween.Stop(gameObject);
            iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(x, y, 0), "time", Random.Range(0f, 0.5f) + queuePos * 0.1f, "easetype", iTween.EaseType.linear));
        }
        Pos = queuePos;
    }
}
