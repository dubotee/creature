﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Lane : List<PartTile> {

    int _lanceId;

    public int LanceId
    {
        get { return _lanceId; }
        set { _lanceId = value; }
    }

    public Lane (int id)
    {
        _lanceId = id;
    }

    internal void Refresh()
    {
        foreach (PartTile tile in this)
        {
            tile.UpdateQueuePostition(this.IndexOf(tile));
        }
    }
}
